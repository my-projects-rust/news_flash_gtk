use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, PartialEq, Deserialize)]
pub enum ArticleListMode {
    All,
    Unread,
    Marked,
}
