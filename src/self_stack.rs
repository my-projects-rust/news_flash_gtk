use glib::{self, IsA, ParamFlags, ParamSpec, ParamSpecObject, Value};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, Picture, Stack, StackTransitionType, Widget,
    WidgetPaintable,
};
use lazy_static::lazy_static;
use std::cell::Cell;

lazy_static! {
    pub static ref PROPERTIES: Vec<ParamSpec> = vec![ParamSpecObject::new(
            // Name
            "child",
            // Nickname
            "child",
            // Short description
            "child",
            // type
            gtk4::Widget::static_type(),
            // The property can be read and written to
            ParamFlags::READWRITE,
        )];
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/self_stack.ui")]
    pub struct SelfStack {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub widget_paintable: TemplateChild<WidgetPaintable>,
        #[template_child]
        pub fake_widget: TemplateChild<Picture>,

        pub animation_duration_ms: Cell<u32>,
    }

    impl Default for SelfStack {
        fn default() -> Self {
            Self {
                stack: TemplateChild::default(),
                widget_paintable: TemplateChild::default(),
                fake_widget: TemplateChild::default(),

                animation_duration_ms: Cell::new(150),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SelfStack {
        const NAME: &'static str = "SelfStack";
        type Type = super::SelfStack;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SelfStack {
        fn properties() -> &'static [ParamSpec] {
            PROPERTIES.as_ref()
        }

        fn set_property(&self, obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "child" => {
                    let child_widget: Widget = value.get().expect("The value needs to be a widget.");
                    obj.set_widget(&child_widget);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "child" => self.stack.child_by_name("widget").to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for SelfStack {}

    impl BoxImpl for SelfStack {}
}

glib::wrapper! {
    pub struct SelfStack(ObjectSubclass<imp::SelfStack>)
        @extends Widget, Box;
}

impl Default for SelfStack {
    fn default() -> Self {
        Self::new()
    }
}

impl SelfStack {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create SelfStack")
    }

    pub fn set_transition_duration(&self, duration: u32) {
        let imp = imp::SelfStack::from_instance(self);
        imp.animation_duration_ms.set(duration);
        imp.stack.set_transition_duration(duration);
    }

    pub fn set_widget<W: IsA<Widget>>(&self, widget: &W) {
        let imp = imp::SelfStack::from_instance(self);
        if let Some(widget) = imp.stack.child_by_name("widget") {
            imp.stack.remove(&widget);
        }
        imp.stack.add_named(widget, Some("widget"));
        imp.widget_paintable.set_widget(Some(widget));
        imp.stack.set_visible_child_name("widget");
    }

    pub fn freeze(&self) {
        let imp = imp::SelfStack::from_instance(self);
        let paintable = imp.widget_paintable.current_image().unwrap();
        imp.fake_widget.set_paintable(Some(&paintable));

        imp.stack.set_transition_duration(0);
        imp.stack.set_visible_child_full("picture", StackTransitionType::None);
    }

    pub fn update(&self, transistion: StackTransitionType) {
        let imp = imp::SelfStack::from_instance(self);
        imp.stack.set_transition_duration(imp.animation_duration_ms.get());
        imp.stack.set_visible_child_full("widget", transistion);
    }
}
