use crate::content_page::ContentPage;
use glib::clone;
use gtk4::prelude::*;
use parking_lot::RwLock;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct ResponsiveLayout {
    pub responsive_state: Arc<RwLock<ResponsiveState>>,
    pub content_page: ContentPage,
}

impl ResponsiveLayout {
    pub fn new(content_page: &ContentPage) -> Self {
        let layout = ResponsiveLayout {
            responsive_state: Arc::new(RwLock::new(ResponsiveState::new())),
            content_page: content_page.clone(),
        };
        layout.setup_signals();
        layout
    }

    fn setup_signals(&self) {
        let major_leaflet = self.content_page.major_leaflet().clone();
        let minor_leaflet = self.content_page.minor_leaflet().clone();

        self.content_page.major_leaflet().connect_folded_notify(clone!(
            @strong self as layout => @default-panic, move |_leaflet|
        {
            if minor_leaflet.is_folded() {
                layout.responsive_state.write().minor_leaflet_folded = true;
                Self::process_state_change(&layout);
            }
            layout.responsive_state.write().major_leaflet_folded = true;
            Self::process_state_change(&layout);
        }));

        self.content_page.minor_leaflet().connect_folded_notify(clone!(
            @strong self as layout => @default-panic, move |_leaflet|
        {
            if !major_leaflet.is_folded() {
                return;
            }
            layout.responsive_state.write().minor_leaflet_folded = true;
            Self::process_state_change(&layout);
        }));

        // swipe back should be handled the same way as back button press
        self.content_page.minor_leaflet().connect_visible_child_name_notify(
            clone!(@strong self as layout => @default-panic, move |leaflet| {
                if leaflet.visible_child_name().map(|s| s.as_str().to_owned()) == Some("feedlist_box".into()) {
                    layout.responsive_state.write().left_button_clicked = true;
                    Self::process_state_change(&layout);
                }
            }),
        );

        self.content_page.article_list_column().back_button().connect_clicked(
            clone!(@strong self as layout => @default-panic, move |_button| {
                layout.responsive_state.write().left_button_clicked = true;
                Self::process_state_change(&layout);
            }),
        );

        self.content_page.articleview_column().back_button().connect_clicked(
            clone!(@strong self as layout => @default-panic, move |_button| {
                layout.responsive_state.write().right_button_clicked = true;
                Self::process_state_change(&layout);
            }),
        );

        self.content_page
            .articleview_column()
            .header_squeezer()
            .connect_visible_child_notify(clone!(@strong self as layout => @default-panic, move |squeezer| {
                layout.responsive_state.write().header_squeezer_hidden = squeezer.visible_child().is_none();
                Self::process_state_change(&layout);
            }));
    }

    pub fn process_state_change(&self) {
        self.content_page
            .articleview_column()
            .footer_revealer()
            .set_reveal_child(self.responsive_state.read().header_squeezer_hidden);

        let major_is_folded = self.content_page.major_leaflet().is_folded();
        let minor_is_folded = self.content_page.minor_leaflet().is_folded();

        self.content_page
            .sidebar_column()
            .headerbar()
            .set_show_end_title_buttons(minor_is_folded);

        self.content_page
            .article_list_column()
            .headerbar()
            .set_show_start_title_buttons(major_is_folded && minor_is_folded);
        self.content_page
            .article_list_column()
            .headerbar()
            .set_show_end_title_buttons(major_is_folded);

        self.content_page
            .articleview_column()
            .headerbar()
            .set_show_start_title_buttons(major_is_folded);

        if self.responsive_state.read().major_leaflet_folded {
            // article view (dis)appeared
            if !major_is_folded {
                self.content_page.articleview_column().back_button().set_visible(false);
                self.content_page.article_list_column().back_button().set_visible(false);
                self.content_page
                    .major_leaflet()
                    .set_visible_child(self.content_page.minor_leaflet());
                self.content_page
                    .minor_leaflet()
                    .set_visible_child(self.content_page.sidebar_column());
                self.content_page.sidebar_column().update_stack().set_visible(false);
            }

            self.responsive_state.write().major_leaflet_folded = false;
            return;
        }

        if self.responsive_state.read().minor_leaflet_folded {
            // article list (dis)appeared
            if !self.content_page.minor_leaflet().is_folded() {
                self.content_page.article_list_column().back_button().set_visible(false);
                self.content_page
                    .minor_leaflet()
                    .set_visible_child(self.content_page.sidebar_column());
            }

            let minor_folded = self.content_page.minor_leaflet().is_folded();
            let offline = self.content_page.state().read().get_offline();

            self.content_page
                .sidebar_column()
                .update_stack()
                .set_visible(minor_folded && !offline);
            self.content_page
                .sidebar_column()
                .offline_button()
                .set_visible(minor_folded && offline);

            self.responsive_state.write().minor_leaflet_folded = false;
            return;
        }

        if self.responsive_state.read().left_button_clicked {
            // left back
            self.content_page
                .minor_leaflet()
                .set_visible_child(self.content_page.sidebar_column());

            self.responsive_state.write().left_button_clicked = false;
            return;
        }

        if self.responsive_state.read().right_button_clicked {
            // right back
            self.content_page
                .major_leaflet()
                .set_visible_child(self.content_page.minor_leaflet());
            self.content_page.articleview_column().back_button().set_visible(false);

            self.responsive_state.write().right_button_clicked = false;
            return;
        }

        if self.responsive_state.read().major_leaflet_selected {
            // article selected
            if self.content_page.major_leaflet().is_folded() {
                self.content_page
                    .major_leaflet()
                    .set_visible_child(self.content_page.articleview_column());
                self.content_page.articleview_column().back_button().set_visible(true);
            }

            self.responsive_state.write().major_leaflet_selected = false;
            return;
        }

        if self.responsive_state.read().minor_leaflet_selected {
            // sidebar selected
            if self.content_page.minor_leaflet().is_folded() {
                self.content_page
                    .minor_leaflet()
                    .set_visible_child(self.content_page.article_list_column());
                self.content_page.article_list_column().back_button().set_visible(true);
            }

            self.responsive_state.write().minor_leaflet_selected = false;
            return;
        }
    }
}

#[derive(Clone, Debug)]
pub struct ResponsiveState {
    pub left_button_clicked: bool,
    pub right_button_clicked: bool,
    pub major_leaflet_selected: bool,
    pub major_leaflet_folded: bool,
    pub minor_leaflet_selected: bool,
    pub minor_leaflet_folded: bool,
    pub header_squeezer_hidden: bool,
}

impl ResponsiveState {
    pub fn new() -> Self {
        ResponsiveState {
            left_button_clicked: false,
            right_button_clicked: false,
            major_leaflet_selected: false,
            major_leaflet_folded: false,
            minor_leaflet_selected: false,
            minor_leaflet_folded: false,
            header_squeezer_hidden: false,
        }
    }
}
